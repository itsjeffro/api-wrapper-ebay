#Getting started#


####Setting the config####
The credentials for the configu must be obtained by signing up for an eBay developer account (free).

Sanbox: http://developer.ebay.com/DevZone/guides/ebayfeatures/Basics/Call-SandboxTesting.html#UsingtheSandbox


```
$config = [
    'apiUrl'    => 'https://api.sandbox.ebay.com/ws/api.dll',
    'siteId'    =>  15,
    'appId'     => 'app123',
    'devId'     => 'dev123',
    'certId'    => 'cert123',
    'authToken' => 'auth123',
	];

$ebayClient = new EbayClient\EbayClient($config);
```


####Get user####
```
$user = $ebayClient->getUser();
```