<?php

namespace EbayClient\Services;

use EbayClient\ApiBuilder;

class GetOrders extends ApiBuilder
{
    /**
     * API: GetOrders
     * @param  array  $params [description]
     * @return [type]         [description]
     */
    public function getOrders($params = [])
    {
        return $this->call('GetOrders', $params);
    }
}
