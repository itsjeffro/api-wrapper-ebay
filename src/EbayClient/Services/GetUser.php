<?php

namespace EbayClient\Services;

use EbayClient\ApiBuilder;

class GetUser extends ApiBuilder
{
    /**
     * API: GetUser
     */
    public function getUser($params = [])
    {
        return $this->call('GetUser', $params);
    }
}
