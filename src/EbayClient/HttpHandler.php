<?php

namespace EbayClient;

class HttpHandler
{
    /**
     * Stored response from request used by getters.
     * @var mixed
     */
    private $response;

    /**
     * Makes a request with specified HTTP method to uri.
     *
     * @param  string  $method  HTTP method
     * @param  string  $uri     URI to make the request to
     * @param  array   $params
     * @return $this
     */
    public function request($method, $uri, $params = [])
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $uri);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $params['headers']);

        curl_setopt($ch, CURLOPT_HEADER, false);

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $params['body']);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $this->response = curl_exec($ch);

        curl_close($ch);

        return $this;
    }

    /**
     * Retrieve body set by request.
     */
    public function getBody()
    {
        return $this->response;
    }
}
