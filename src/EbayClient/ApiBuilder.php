<?php

namespace EbayClient;

use EbayClient\HttpHandler;

class ApiBuilder
{
    /**
     * The uri for either sandbox or production api.
     * @var string
     */
    public $apiUrl;

    /**
     * [$compatLevel description]
     * @var intger
     */
    public $compatLevel = 967;

    /**
     * Site country id. default is Australia.
     * @var integer
     */
    public $siteId = 15;

    /**
     * [$appId description]
     * @var string
     */
    public $appId;

    /**
     * [$devId description]
     * @var string
     */
    public $devId;

    /**
     * [$certId description]
     * @var string
     */
    public $certId;

    /**
     * [$authToken description]
     * @var string
     */
    public $authToken;

    /**
     * Http curl handler.
     * @var object
     */
    public $http;

    /**
     * Header containing the config passed to the ebay api.
     * @var array
     */
    public $headers = [];

    /**
     * Instantiate.
     * @param array $config Values used to access the ebay api.
     */
    public function __construct($config = [])
    {
        $this->apiUrl    = (array_key_exists('apiUrl', $config)) ? $config['apiUrl'] : null;
        $this->siteId    = (array_key_exists('siteId', $config)) ? $config['siteId'] : null;
        $this->appId     = (array_key_exists('appId', $config)) ? $config['appId'] : null;
        $this->devId     = (array_key_exists('devId', $config)) ? $config['devId'] : null;
        $this->certId    = (array_key_exists('certId', $config)) ? $config['certId'] : null;
        $this->authToken = (array_key_exists('authToken', $config)) ? $config['authToken'] : null;

        $this->http = new HttpHandler;
    }

    /**
     * Our request with the chosen call and params.
     *
     * @param  string $callname  The eBay api endpoint called.
     * @param  array  $params    The call endpoints available params.
     * @return object
     */
    public function call($callname, $params = [])
    {
        $headers = [
            'Content-type: application/xml',
            'Accept: application/xml',
            'X-EBAY-API-COMPATIBILITY-LEVEL: '.$this->compatLevel,
            'X-EBAY-API-DEV-NAME: '.$this->devId,
            'X-EBAY-API-APP-NAME: '.$this->appId,
            'X-EBAY-API-CERT-NAME: '.$this->certId,
            'X-EBAY-API-SITEID: '.$this->siteId,
            'X-EBAY-API-CALL-NAME: '.$callname,
            ];
            
        $xml = $this->xmlRequest($callname, $params);

        $response = $this->http->request('POST', $this->apiUrl, [
            'headers' => $headers,
            'body' => $xml,
            ]);

        return $this->response($response->getBody());
    }

    /**
     * Convert ebay response from XML to object.
     *
     * @param  xml  $response  The response received from our eBay api request.
     * @return object
     */
    public function response($response)
    {
        $xmlObject = new \SimpleXMLElement($response);

        return json_decode(json_encode($xmlObject));
    }

    /**
     * Convert api request array to an XML format.
     *
     * @param  string  $callname  The api name we're making a call to.
     * @param  array   $params    Options for the api call.
     * @return string
     */
    public function xmlRequest($callname, $params = [])
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<'.$callname.'Request xmlns="urn:ebay:apis:eBLBaseComponents">';

        if ($this->authToken) {
            $xml .= '<RequesterCredentials><eBayAuthToken>'.$this->authToken.'</eBayAuthToken></RequesterCredentials>';
        }

        $xml .= $this->xmlInput($params);

        $xml .= '</'.$callname.'Request>';

        return $xml;
    }

    /**
     * If the value is an array and not a string, then continue converting to xml
     * until the value is no longer an array. Used for nested options.
     *
     * @param  array  $input  Api request options.
     * @return string
     */
    public function xmlInput(array $input)
    {
        return array_reduce(array_keys($input), function ($carry, $key) use ($input) {
            if (is_string($input[$key])) {
                return "<{$key}>{$input[$key]}</{$key}>";
            }
            
            if (is_array($input[$key])) {
                return "<{$key}>{$this->xmlInput($input[$key])}</{$key}>";
            }
            
            throw new \InvalidArgumentException('Provided input is not valid.');
        });
    }
}
