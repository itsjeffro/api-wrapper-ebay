<?php
use PHPUnit\Framework\TestCase;
use EbayClient\ApiBuilder;

class CredentialTest extends TestCase
{
    public function testCredentials()
    {
        $config = [
            'apiUrl'    => 'http://api',
            'siteId'    => 'site123',
            'appId'     => 'app123',
            'devId'     => 'dev123',
            'certId'    => 'cert123',
            'authToken' => 'auth123',
            ];

        $apiBuilder = new ApiBuilder($config);

        $this->assertEquals('http://api', $apiBuilder->apiUrl);
        $this->assertEquals('site123', $apiBuilder->siteId);
        $this->assertEquals('app123', $apiBuilder->appId);
        $this->assertEquals('dev123', $apiBuilder->devId);
        $this->assertEquals('cert123', $apiBuilder->certId);
        $this->assertEquals('auth123', $apiBuilder->authToken);
    }
}
