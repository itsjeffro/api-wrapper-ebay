<?php
use PHPUnit\Framework\TestCase;
use EbayClient\ApiBuilder;

class XmlInputRequestTest extends TestCase
{
    public function testXmlNestingInput()
    {
        $apiBuilder = new ApiBuilder();

        $input = [
            'Pagination' => [
                'EntriesPerPage' => 10
                ]
            ];

        $inputRequest = $apiBuilder->xmlInput($input);

        $this->assertEquals('<Pagination><EntriesPerPage>10</EntriesPerPage></Pagination>', $inputRequest);
    }
}